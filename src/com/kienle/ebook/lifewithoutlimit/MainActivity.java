package com.kienle.ebook.lifewithoutlimit;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;

import com.artifex.mupdf.MuPDFActivity;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		if (copyFileToInternal()) {
//		Uri path = Uri.parse("android.resource://com.kienle.ebook.lifewithoutlimit/" + R.raw.life_without_limits);
//		Uri path = Uri.fromFile(new File("file://android_asset/Life_Without_Limits.pdf"));
//		Uri path = Uri.parse("file:///android:asset/Life_Without_Limits.pdf");
//		Uri path = Uri.parse("android.resource://" + getPackageName() + "/raw/life_without_limits");
//		File file = new File(url.toString());
		File internalFile = getFileStreamPath("life_without_limits.pdf");
        Uri internal = Uri.fromFile(internalFile);
		Intent intent = new Intent(MainActivity.this, MuPDFActivity.class);
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(internal, "application/pdf");

//        try {
            startActivity(intent);
//        } catch (ActivityNotFoundException e) {
//            Toast.makeText(MainActivity.this, "NO Pdf Viewer", Toast.LENGTH_SHORT).show();
//        }
		}
	}
	
	private boolean copyFileToInternal() {
	    try {
	        InputStream is = getAssets().open("Life_Without_Limits.pdf");

	        File cacheDir = getCacheDir();
	        File outFile = new File(cacheDir, "Life_Without_Limits.pdf");

	        OutputStream os = new FileOutputStream(outFile.getAbsolutePath());

	        byte[] buff = new byte[1024];
	        int len;
	        while ((len = is.read(buff)) > 0) {
	            os.write(buff, 0, len);
	        }
	        os.flush();
	        os.close();
	        is.close();
	        return true;
	    } catch (IOException e) {
	        e.printStackTrace(); // TODO: should close streams properly here
	    }
	    
	    return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
